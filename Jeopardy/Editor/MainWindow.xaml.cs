﻿using Microsoft.Win32;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Editor
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Game game = new Game();
        Round editRound;
        Question editQuestion;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            roundCountBox.SelectedIndex = 0;
        }

        private void cellMouseDown(object s, object e)
        {
            var tb = s as TextBox;
            int row = Grid.GetRow(tb) - 1;
            int column = Grid.GetColumn(tb) - 1;
            var eq = editRound.Questions[row, column];
            if (eq.Name==null || eq.Name == "")
            {
                eq.Name = editRound.Prices[column].ToString();
                tb.Text = eq.Name;
            }
            EditQuestion(eq);
        }

        private void EditQuestion(Question question)
        {
            editQuestion = (Question)DataContext;
            DataContext = null;
            editQuestion = question;
            DataContext = question;
        }

        /*
            уравниваем кол-во раундов с комбобоксом
        */
        private void roundCountBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var sr = roundCountBox.SelectedIndex;
            if (sr>-1)
            {
                var dif = sr - game.Rounds.Count+1;
                if (dif>0)
                {
                    for (int i = 0; i < dif; i++) game.Rounds.Add(new Round());
                }
                else if (dif<0)
                {
                    for (int i = 0; i < Math.Abs(dif); i++)
                    {
                        game.Rounds.Remove(game.Rounds[game.Rounds.Count - 1]);
                    }
                }
                roundBox.ItemsSource = GetRounds(sr);
                roundBox.SelectedIndex = 0;
            }
        }

        private void roundBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var sr = roundBox.SelectedIndex;
            if (sr > -1)
            {
                editRound = game.Rounds[sr];
                Update();
            }
        }

        private void Update()
        {
            if (editRound.Themes == null || editRound.Themes.Count == 0)
            {
                editRound.Themes = new List<string>(Round.Size);
                editRound.Prices = new List<int>(Round.Size);
                editRound.Questions = new Question[Round.Size, Round.Size];
                for (int i = 0; i < Round.Size; i++)
                {
                    editRound.Prices.Add((i + 1) * 100);
                    editRound.Themes.Add("");
                    for (int j = 0; j < Round.Size; j++)
                    {
                        editRound.Questions[i, j] = new Question();
                    }
                }
            }

            price1.Text = editRound.Prices[0].ToString();
            price2.Text = editRound.Prices[1].ToString();
            price3.Text = editRound.Prices[2].ToString();
            price4.Text = editRound.Prices[3].ToString();
            price5.Text = editRound.Prices[4].ToString();

            theme1.Text = editRound.Themes[0];
            theme2.Text = editRound.Themes[1];
            theme3.Text = editRound.Themes[2];
            theme4.Text = editRound.Themes[3];
            theme5.Text = editRound.Themes[4];

            foreach (var c in grid.Children)
            {
                var tb = c as TextBox;
                if (tb != null)
                {
                    var i = Grid.GetRow(tb);
                    var j = Grid.GetColumn(tb);
                    if ((i > 0) && (j > 0) 
                        && (i<6) && (j<6))
                    {
                        tb.Text = editRound.Questions[i-1, j-1].Name;
                    }
                }
            }
        }

        private void Save()
        {
            try {
                editRound.Prices[0] = int.Parse(price1.Text);
                editRound.Prices[1] = int.Parse(price2.Text);
                editRound.Prices[2] = int.Parse(price3.Text);
                editRound.Prices[3] = int.Parse(price4.Text);
                editRound.Prices[4] = int.Parse(price5.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Цена должна быть целым числом");
            }
            editRound.Themes[0] = theme1.Text;
            editRound.Themes[1] = theme2.Text;
            editRound.Themes[2] = theme3.Text;
            editRound.Themes[3] = theme4.Text;
            editRound.Themes[4] = theme5.Text;
        }

        private IEnumerable<int> GetRounds(int q)
        {
            for (int i = 0; i <= q; i++)
            {
                yield return i + 1;
            }
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            Save();

            for (int i = 0; i <game.Rounds.Count; i++)
            {
                game.Rounds[i].Name = string.Format("{0} раунд", i+1);
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = "game";
            saveFileDialog.DefaultExt = ".json";
            saveFileDialog.Filter = "Jeopardy files (*.json)|*.json|All files (*.*)|*.*";
            saveFileDialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            bool? result = saveFileDialog.ShowDialog();
            if (result == true)
            {
                try
                {
                    GameManager.Write(saveFileDialog.FileName, game);
                }
                catch (Exception)
                {
                    MessageBox.Show("Не удалось скопировать изображения или записать файл");
                }
            }
        }

        private void load_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Jeopardy files (*.json)|*.json|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if (openFileDialog.ShowDialog() == true)
            {
                game = GameManager.Read(openFileDialog.FileName);
            }
            editRound = game.Rounds[roundBox.SelectedIndex];
            Update();
        }

        private void CheckImage_Click(object sender, RoutedEventArgs e)
        {
            var q = editQuestion;
            if (q.ContentType == ContentType.Picture)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image files (*.*)|*.*";
                openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

                if (openFileDialog.ShowDialog() == true)
                {
                    q.Content = openFileDialog.FileName;
                    EditQuestion(q);
                }
            }
        }
    }
}
