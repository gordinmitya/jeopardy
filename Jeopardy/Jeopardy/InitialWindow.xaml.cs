﻿using Microsoft.Win32;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Jeopardy
{
    public partial class InitialWindow : Window
    {
        public InitialWindow()
        {
            InitializeComponent();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            if (comandNameBox.Text == "") return;
            listBox.Items.Add(comandNameBox.Text);
            comandNameBox.Text = "";
            if (App.game != null) startButton.IsEnabled = true;
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            App.CommandCount = listBox.Items.Count;
            App.CommandNames = listBox.Items.OfType<string>().ToList();
            App.CommandNames.ForEach((r) =>
            {
                App.CommandRaiting.Add(0);
            });
            new MainWindow().Show();
            Close();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Jeopardy files (*.json)|*.json|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            if (App.DEBAG) openFileDialog.FileName = @"C:\dev\jeopardy\Jeopardy\Editor\bin\Debug\game.json";
            if (App.DEBAG || openFileDialog.ShowDialog() == true)
            {
                App.imageDir = new Uri(GameManager.getImagesFolder(openFileDialog.FileName)+"/");
                App.game = GameManager.Read(openFileDialog.FileName);
            }
            if (listBox.Items.Count>0) startButton.IsEnabled = true;
        }

        private IEnumerable<string> GetComandNames(int count)
        {
            for (int i = 1; i <= count; i++)
            {
                yield return string.Format("Команда {0}", i);
            }
        }
    }
}
