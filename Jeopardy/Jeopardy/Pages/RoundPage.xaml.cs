﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Jeopardy.Pages
{
    public partial class RoundPage : Page
    {
        private Round round;
        public RoundPage(Round r)
        {
            round = r;
            InitializeComponent();

            theme1.Text = round.Themes[0];
            theme2.Text = round.Themes[1];
            theme3.Text = round.Themes[2];
            theme4.Text = round.Themes[3];
            theme5.Text = round.Themes[4];

            price1.Text = round.Prices[0].ToString();
            price2.Text = round.Prices[1].ToString();
            price3.Text = round.Prices[2].ToString();
            price4.Text = round.Prices[3].ToString();
            price5.Text = round.Prices[4].ToString();
        }

        private void cellMouseDown(object s, object e)
        {
            var tb = s as TextBox;
            tb.IsEnabled = false;

            int i = Grid.GetRow(tb) - 1;
            int j = Grid.GetColumn(tb) - 1;

            var q = round.Questions[i, j];

            NavigationService.Navigate(new QuestionPage(q, round.Prices[j]));
        }
    }
}
