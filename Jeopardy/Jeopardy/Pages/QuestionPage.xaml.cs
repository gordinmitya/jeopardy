﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Jeopardy.Pages
{
    public partial class QuestionPage : Page
    {
        Question question;
        int price;
        bool questionShow = false;

        public QuestionPage(Question q, int p)
        {
            price = p;
            question = q;
            InitializeComponent();
            answer.Text = q.Answer;

            switch (q.Type)
            {
                case QuestionType.Standart:
                    showQuestion();
                    break;
                case QuestionType.CatInBag:
                    showCatInBag();
                    break;
                case QuestionType.Auction:
                    showAuction();
                    break;
            }
        }
        private void showCatInBag()
        {
            showPlaceholder(new Uri(@"/Jeopardy;component/Images/auction.png", UriKind.RelativeOrAbsolute));
            App.playSound("auk.wav");
        }
        private void showAuction()
        {
            showPlaceholder(new Uri(@"/Jeopardy;component/Images/catInBag.png", UriKind.RelativeOrAbsolute));
            App.playSound("Cat.wav");
        }
        private void showPlaceholder(Uri uri)
        {
            placeHolderImage.Visibility = Visibility.Visible;

            BitmapImage bi3 = new BitmapImage();
            bi3.BeginInit();
            bi3.UriSource = uri;
            bi3.EndInit();
            placeHolderImage.Source = bi3;
        }
        private void placeHolderClick(object sender, MouseButtonEventArgs e)
        {
            placeHolderImage.Visibility = Visibility.Collapsed;
            showQuestion();
        }
        private void showQuestion()
        {
            questionShow = true;
            App.playSound("button.wav");
            if (question.ContentType == ContentType.Text)
            {
                textContent.Visibility = Visibility.Visible;
                questionText.Text = question.Content;
            }
            else
            {
                imageContent.Visibility = Visibility.Visible;
                BitmapImage bi3 = new BitmapImage();
                bi3.BeginInit();
                bi3.UriSource = new Uri(App.imageDir, question.Content);
                bi3.EndInit();
                imageContent.Source = bi3;
            }
        }

        private void showAnswer(object sender, MouseButtonEventArgs e)
        {
            if (!questionShow) return;
            if (answer.Visibility == Visibility.Hidden)
            {
                answer.Visibility = Visibility.Visible;
            }
            else
            {
                NavigationService.Navigate(AddRatingPage.GetPage(price));
            }
        }
    }
}
