﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Jeopardy.Pages
{
    public partial class GamePage : Page
    {
        List<RoundPage> pages = new List<RoundPage>();
        List<TextBlock> scores = new List<TextBlock>();

        public GamePage()
        {
            InitializeComponent();
            var leftMargin = new Thickness(0, 0, 15, 0);
            for (int i = 0; i < App.game.Rounds.Count; i++)
            {
                pages.Add(new RoundPage(App.game.Rounds[i]));

                var tb = new TextBlock() { Text = App.game.Rounds[i].Name, FontSize = 16, DataContext = i, Margin = leftMargin };
                tb.PreviewMouseLeftButtonDown += navigationButtonClick;
                App.navigationBar.Children.Add(tb);
            }

            TextBlock tb2 = new TextBlock() { Text = "Рейтинги", FontSize = 16, DataContext = -1, Margin = leftMargin };
            tb2.PreviewMouseLeftButtonDown += navigationButtonClick;
            App.navigationBar.Children.Add(tb2);

            var marginLeft = new Thickness(10, 0, 0, 0);
            var bigMarginRight = new Thickness(0, 15, 25, 0);
            for (int i = 0; i < App.CommandCount; i++)
            {
                var sp = new StackPanel() { Orientation = Orientation.Horizontal, Margin = bigMarginRight };
                
                var tbn = new TextBlock() { Text = App.CommandNames[i], FontSize = 34, };
                sp.Children.Add(tbn);
                
                var tbv = new TextBlock() { Text = "0", FontSize = 36, Margin = marginLeft };
                scores.Add(tbv);
                sp.Children.Add(tbv);

                comandScores.Children.Add(sp);
            }
        }

        private void navigationButtonClick(object sender, MouseButtonEventArgs e)
        {
            var s = sender as TextBlock;
            int id = (int)s.DataContext;
            if (id >= 0)
            {
                frame.Navigate(pages[id]);
            }
            else
            {
                frame.Navigate(AddRatingPage.GetPage(0, (Page)frame.Content));
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            frame.Navigate(pages.First());
            AddRatingPage.updateMe = UpdateScores;
        }

        public void UpdateScores()
        {
            for (var i=0; i<App.CommandCount; i++)
            {
                scores[i].Text = App.CommandRaiting[i].ToString();
            }
        }
    }
}