﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Jeopardy.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddRatingPage.xaml
    /// </summary>
    public partial class AddRatingPage : Page
    {
        private List<TextBlock> names;
        private List<TextBox> values;

        public AddRatingPage()
        {
            names = new List<TextBlock>();
            values = new List<TextBox>();

            InitializeComponent();

            generateGrid();
        }

        private void generateGrid()
        {
            listGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            listGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            for (int i = 0; i < App.CommandNames.Count; i++)
            {
                listGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

                names.Add(new TextBlock() { Text = App.CommandNames[i], FontSize = 40, VerticalAlignment = VerticalAlignment.Center, FontWeight = FontWeights.Bold });
                names[i].PreviewMouseDown += nameClick;
                Grid.SetRow(names[i], i);
                listGrid.Children.Add(names[i]);

                values.Add(new TextBox() { Text = App.CommandRaiting[i].ToString(), FontSize = 40, VerticalAlignment = VerticalAlignment.Center });
                values[i].TextChanged += textChanged;
                Grid.SetRow(values[i], i);
                Grid.SetColumn(values[i], 1);
                listGrid.Children.Add(values[i]);
            }
        }

        private void nameClick(object sender, MouseButtonEventArgs e)
        {
            int score;
            int position = names.IndexOf(sender as TextBlock);
            var tb = values[position];
            if (int.TryParse(tb.Text, out score))
            {
                //прибавляем по левой
                if (e.ChangedButton == MouseButton.Left)
                {
                    score += price;
                }
                //отнимаем по правой
                else
                {
                    score -= price;
                }
                tb.Text = score.ToString();
            }
        }

        private void Save()
        {
            try
            {
                for (int i = 0; i < App.CommandCount; i++)
                {
                    App.CommandRaiting[i] = int.Parse(values[i].Text);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Баллы команды - целое число"); 
            }
        }

        private void textChanged(object sender, TextChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }
        public static Action updateMe;
        private static AddRatingPage thisPage;
        private static int price = 0;
        private static Page navigateBack;
        public static AddRatingPage GetPage(int p = 0, Page back = null)
        {
            if (thisPage == null)
            {
                thisPage = new AddRatingPage();
            }
            navigateBack = back;
            price = p;
            return thisPage;
        }

        private void continueClick(object sender, RoutedEventArgs e)
        {
            Save();
            if (updateMe != null) updateMe();
            if (navigateBack != null)
            {
                NavigationService.Navigate(navigateBack);
            }
            else
            {
                NavigationService.GoBack();
                NavigationService.GoBack();
            }
        }
    }
}
