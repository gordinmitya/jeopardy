﻿using Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Jeopardy
{
    public partial class App : Application
    {
        public static StackPanel navigationBar;

        public static bool DEBAG = true;
        public static Game game;
        public static int CommandCount = 5;
        public static List<string> CommandNames;
        public static List<int> CommandRaiting = new List<int>();
        public static Uri imageDir;

        public static void playSound(string name)
        {
            try {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer();
                player.SoundLocation = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Sounds", name);
                player.Load();
                player.Play();
            }
            catch (Exception)
            {

            }
        }
    }
}