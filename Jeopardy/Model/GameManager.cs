﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public static class GameManager
    {
        public static Game Read(string fileName)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                StreamReader sr = new StreamReader(fs);
                return JsonConvert.DeserializeObject<Game>(sr.ReadToEnd());
            }
        }

        public static void Write(string fileName, Game game)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                StreamWriter sw = new StreamWriter(fs);
                copyImages(fileName, game);
                sw.Write(JsonConvert.SerializeObject(game));
                sw.Flush();
            }
        }

        public static string getImagesFolder(string jsonGameFile)
        {
            return Path.Combine(
                Path.GetDirectoryName(jsonGameFile), 
                Path.GetFileNameWithoutExtension(jsonGameFile)
                );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName">полный путь до файла</param>
        /// <param name="game"></param>
        private static void copyImages(string fileName, Game game)
        {
            var dir = Path.GetDirectoryName(fileName);
            var file = Path.GetFileNameWithoutExtension(fileName);
            var imgDir = getImagesFolder(fileName);
            Directory.CreateDirectory(imgDir);

            foreach (var r in game.Rounds)
            {
                for (int i = 0; i < Round.Size; i++)
                {
                    for (int j = 0; j < Round.Size; j++)
                    {
                        var q = r.Questions[i, j];
                        if (q.ContentType == ContentType.Picture && q.Content != null && q.Content != "")
                        {
                            if (Path.GetDirectoryName(q.Content) == "") continue;

                            var fn = Path.GetFileName(q.Content);
                            File.Copy(q.Content, Path.Combine(imgDir, fn));
                            q.Content = fn;
                        }
                    }
                }
            }
        }
    }
}
