﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Round
    {
        public static readonly int Size = 5;

        public Round()
        {
        }

        public string Name { get; set; }
        public List<string> Themes { get; set; }
        public List<int> Prices { get; set; }
        public Question[,] Questions { get; set; }
    }
}
