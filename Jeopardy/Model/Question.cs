﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public enum QuestionType
    {
        Standart,
        Auction,
        CatInBag
    }

    public enum ContentType
    {
        Text,
        Picture
    }

    public class Question
    {
        public QuestionType Type { get; set; }
        public string Name { get; set; }
        public ContentType ContentType { get; set; }
        public string Content { get; set; }
        public string Answer { get; set; }
    }
}
