﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Game
    {
        public Game()
        {
            Rounds = new List<Round>();
            FinalQuestions = new List<Question>();
        }

        public List<Round> Rounds { get; set; }
        public List<Question> FinalQuestions { get; set; }
    }
}
